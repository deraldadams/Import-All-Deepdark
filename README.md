# Import-All-Deepdark
Install my deepdark themes all at once from github. May the dark be kinder on thine eyes.

+ [DuckDuckGo](https://gitlab.com/RaitaroH/DuckDuckGo-DeepDark/raw/master/DuckDuckGoDeepDark.user.css)
+ [Gmail](https://gitlab.com/RaitaroH/Gmail-DeepDark/raw/master/GmailDeepDark.user.css)
+ [Jisho](https://gitlab.com/RaitaroH/Jisho-DeepDark/raw/master/JishoDeepDark.user.css)
+ [KissAnime](https://gitlab.com/RaitaroH/KissAnime-DeepDark/raw/master/KissAnimeDeepDark.user.css)
+ [LinuxMintBlog](https://gitlab.com/RaitaroH/LinuxMint_Blog-Deepdark/raw/master/LinuxMintBlog-DeepDark.user.css)
+ [MyAnimeList](https://gitlab.com/RaitaroH/MyAnimeList-DeepDark/raw/master/MyAnimeListDeepDark.user.css)
+ [MAL_AnimePlus](https://gitlab.com/RaitaroH/MAL_AnimePlusGraph-DeepDark/raw/master/MAL_AnimePlusGraphDeepDark.user.css)
+ [Masteranime](https://gitlab.com/RaitaroH/Masteranime-DeepDark/raw/master/Masteranime-DeepDark.user.css)
+ [OpenUserCss](https://rawgit.com/OpenUserCSS/OpenUserCSS-DeepDark/master/OpenUserCSSDeepDark.user.css)
+ [ProtonMail](https://gitlab.com/RaitaroH/ProtonMail-DeepDark/raw/master/ProtonMailDeepDark.user.css)
+ [Qwant](https://gitlab.com/RaitaroH/Qwant-DeepDark/raw/master/Qwant.user.css)
+ [Syncthing](https://gitlab.com/RaitaroH/Syncthing-DeepDark/raw/master/SyncthingDeepDark.user.css)
+ [Stylus](https://gitlab.com/RaitaroH/Stylus-DeepDark/raw/master/StylusDeepDark.user.css)
+ [Userstyles](https://gitlab.com/RaitaroH/Userstyles-DeepDark/raw/master/UserstylesDeepDark.user.css)
+ [Vidme](https://gitlab.com/RaitaroH/Vidme-DeepDark/raw/master/VidmeDeepDark.user.css)
+ [WhatsApp](https://gitlab.com/RaitaroH/WhatsApp-DeepDark/raw/master/WhatsAppDeepDark.user.css)
+ [YouTubeClassic](https://gitlab.com/RaitaroH/YouTube-DeepDark/raw/master/YouTubeDeepDarkClassic.user.css)
+ [YouTubeMaterial](https://gitlab.com/RaitaroH/YouTube-DeepDark/raw/master/YouTubeDeepDarkMaterial.user.css)

The easy color switcher:
+ [Switcher](https://gitlab.com/RaitaroH/Import-All-Deepdark/raw/master/switcher.user.css)

Extra stuff:
+ [Hidden Password](https://gitlab.com/RaitaroH/Hidden-Password/master/HiddenPassword.user.css)
+ [DarkThemeFoxFix](https://gitlab.com/RaitaroH/DarkThemeFoxFix/master/DarkThemeFoxFix.user.css)

### Advantages
+ Easily install all my themes.         
+ Choose only what themes you want.      
+ Have the most up-to-date versions of the themes (I don't upload to userstyles every single release).            
+ Allows color changing across all the themes for uniformity and consistency. (If you use the switcher.user.css)                 
+ Allows easily color picking using Sylus's UI.
+ Changes to the color palettes will not be overwritten by updates.         
+ Overwrite the color palettes of the individual themes.         
+ It can also work with other themes as long as they have a place to import from such as github. (using the @imports)         

### Disadvantages - IF YOU INSTALL THEM ONE BY ONE USING STYLUS, THEN, THIS DOESN'T APPLY
+ You cannot modify or see the code of the themes inside Stylus.         
+ Possible lag before the theme is applied on the page.         